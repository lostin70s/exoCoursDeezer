import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-fiche-morceau',
  templateUrl: './fiche-morceau.component.html',
  styleUrls: ['./fiche-morceau.component.css']
})
export class FicheMorceauComponent implements OnInit {
 
 bckcolo:any;
 imageSource:string="";

 @Input() data:any;
  constructor() {

    
   }

  ngOnInit() {
    console.log(this.data.album.cover)
    this.imageSource=this.data.album.cover;
  }
  onHover(event)
  {
    if( event=="enter")
    {
      this.imageSource=this.data.artist.picture;
    }
    else
    {
      this.imageSource=this.data.album.cover;
    }
  }

}
