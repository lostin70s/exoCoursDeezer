import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { DeezerService } from '../../services/deezer.service';



@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {
  marecherche:string="";
  data:any=[];
  @Output() trackId=new EventEmitter;
  constructor(private deezer:DeezerService) { }

  ngOnInit() {
  }

  onKeyPressed()
  {
      
      this.deezer.search(this.marecherche).then((data:any)=>{
        
        this.data = data.data.filter((value) => {
          if (value.type == "track")
            return true;
          else
            return false;
        });
      })
  }
  onClick(id)
  {
      this.trackId.emit(id);
      this.data=[];

  }
}
