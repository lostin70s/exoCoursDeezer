import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';
@Injectable()
export class DeezerService {

  constructor(private http : HttpClient) { }

  search(maRecherche : string){
    return new Promise((resolve,reject)=>{

        this.http.get("https://api.deezer.com/search",{params: new HttpParams().set('q',maRecherche)})
        .subscribe(
          data=>{
            console.log(JSON.stringify(data));
            resolve(data);
          }
          ,err=>{
            console.log(JSON.stringify(err));
            reject(err);
          }
        )
    })
  }

  track(monID : string){
    return new Promise((resolve,reject)=>{

        this.http.get(" https://api.deezer.com/track/"+monID)
        .subscribe(
          data=>{
            console.log(JSON.stringify(data));
            resolve(data);
          }
          ,err=>{
            console.log(JSON.stringify(err));
            reject(err);
          }
        )
    })
  }

}
