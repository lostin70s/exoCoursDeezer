import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'powtwo'
})
export class PowtwoPipe implements PipeTransform {

  transform(value: number, args: {pow:number,add:string}): string {
    let pow:number=args? args.pow || 2 : 2;
    let plus:string=args? args.add || "": "";
       let output=Math.pow(pow,Math.ceil(Math.log(value)/Math.log(pow)))+plus;
        return output;
    }

}
