import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-fiche-morceau',
  templateUrl: './fiche-morceau.component.html',
  styleUrls: ['./fiche-morceau.component.css']
})
export class FicheMorceauComponent implements OnInit {

  data:any;
  monIndex:number;
  monStyle:string;
  monInput:string;
  tableau:Array<any>=[];
  maintenant:Date;
  model: NgbDateStruct;
 Pickeddate: {year: number, month: number};
  


  constructor(private route: ActivatedRoute) {
    this.maintenant=new Date(); 
    this.monStyle="color:blue"
    this.monInput="hello"
    this.tableau.push({name:"choix1",val:1});
    this.tableau.push({name:"choix2",val:4});
    this.tableau.push({name:"choix3",val:2});
this.monIndex=0;
    this.data = {
      "id": 3135556,
      "readable": true,
      "title": "Harder Better Faster Stronger",
      "title_short": "Harder Better Faster Stronger",
      "title_version": "",
      "isrc": "GBDUW0000059",
      "duration": 224,
      "track_position": 4,
      "disk_number": 1,
      "rank": 727696,
      "release_date": "2001-03-07",
      "explicit_lyrics": false,
      "preview": "https://e-cdns-preview-5.dzcdn.net/stream/51afcde9f56a132096c0496cc95eb24b-4.mp3",
      "bpm": 123,
      "gain": -12.4,
      "artist": {
        "id": 27,
        "name": "Daft Punk",
        "link": "https://www.deezer.com/artist/27",
        "picture": "https://api.deezer.com/artist/27/image",
      },
      "album": {
        "id": 302127,
        "title": "Discovery",
        "cover": "https://api.deezer.com/album/302127/image",
        "release_date": "2001-03-07",
      }
    }

  }

  ngOnInit() {
    this.route.queryParams
    .subscribe((data) => {
    alert(JSON.stringify(data));
    });

  }

  onClick(event)
  {

    console.log(event)
    this.data.cd=<any>{};
    this.data.cd.title="j'ai ajouter un titre"
  }
  onVote(element,i)
  {
    //alert(element.val);
  this.monIndex=i;

  
  }
  onMonitemCLick(val:string ,i)
  {
    this.tableau[i].name+=val;
  }


}
