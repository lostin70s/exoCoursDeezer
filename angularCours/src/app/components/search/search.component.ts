import { DeezerService } from './../../services/deezer.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private deezer:DeezerService) { }

  ngOnInit() {
      this.deezer.search("EMINEM").then((data)=>{
        console.log(JSON.stringify(data));
      })
  }

}
